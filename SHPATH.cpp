// SHPATH - The Shortest Path
// http://www.spoj.com/problems/SHPATH/

/*
SHPATH - The Shortest Path
#shortest-path #dijkstra-s-algorithm

You are given a list of cities. Each direct connection between two cities has its transportation cost (an integer bigger than 0). The goal is to find the paths of minimum cost between pairs of cities. Assume that the cost of each path (which is the sum of costs of all direct connections belongning to this path) is at most 200000. The name of a city is a string containing characters a,...,z and is at most 10 characters long.

Input
 

s [the number of tests <= 10]
n [the number of cities <= 10000]
NAME [city name]
p [the number of neighbours of city NAME]
nr cost [nr - index of a city connected to NAME (the index of the first city is 1)]
           [cost - the transportation cost]
r [the number of paths to find <= 100]
NAME1 NAME2 [NAME1 - source, NAME2 - destination]
[empty line separating the tests]
 

Output
 

cost [the minimum transportation cost from city NAME1 to city NAME2 (one per line)]
 

Example
 

Input:
1
4
gdansk
2
2 1
3 3
bydgoszcz
3
1 1
3 1
4 4
torun
3
1 3
2 1
4 1
warszawa
2
2 4
3 1
2
gdansk warszawa
bydgoszcz warszawa

Output:
3
2
*/

#include <iostream> // std::cout; std::cin
#include <fstream> // std::fstream::open; std::fstream::close;
#include <ctime>
#include <cstdlib> // rand
#include <cassert> // assert
#include <cctype> // isalnum; isalpha; isdigit; islower; isupper; isspace; tolower; toupper
#include <cmath> // pow; sqrt; round; fabs; abs; log
#include <climits> // INT_MIN; INT_MAX; LLONG_MIN; LLONG_MAX; ULLONG_MAX
#include <cfloat> // DBL_EPSILON; LDBL_EPSILON
#include <cstring> // std::memset
#include <algorithm> // std::swap; std::max; std::min; std::min_element; std::max_element; std::minmax_element; std::next_permutation; std::prev_permutation; std::nth_element; std::sort; std::lower_bound; std::upper_bound; std::reverse
#include <limits> // std::numeric_limits<int>::min; std::numeric_limits<int>::max; std::numeric_limits<double>::epsilon; std::numeric_limits<long double>::epsilon;
#include <numeric> // std::accumulate; std::iota
#include <string> // std::to_string; std::string::npos; std::stoul; std::stoull; std::stoi; std::stol; std::stoll; std::stof; std::stod; std::stold; 
#include <list> // std::list::merge; std::list::splice; std::list::merge; std::list::unique; std::list::sort
#include <bitset>
#include <vector>
#include <deque>
#include <stack> // std::stack::top; std::stack::pop; std::stack::push
#include <queue> // std::queue::front; std::queue::back; std::queue::pop; std::queue::push; std::priority_queue; std::priority_queue::top; std::priority_queue::push; std::priority_queue::pop
#include <set> // std::set::count; std::set::find; std::set::equal_range; std::set::lower_bound; std::set::upper_bound
#include <map> // std::map::count; std::map::find; std::map::equal_range; std::map::lower_bound; std::map::upper_bound
#include <unordered_set>
#include <unordered_map>
#include <utility> // std::pair; std::make_pair
#include <iterator>
#include <functional> // std::less<int>; std::greater<int>
using namespace std;

class Graph {
public:
	Graph(const int n) {
		h.clear();
		g.clear();
		g.resize(n);
	}
	void insert(string name, int b, int e, const int w) {
		b--;
		e--;
		h[name] = b;
		g.at(b).push_back(make_pair(e, w));
	}
	int Dijkstra(string a, string b) {
		int src = h.at(a), dist = h.at(b);
		vector<int> d(h.size(), INT_MAX);
		set<pair<int, int>> rbtree;
		rbtree.insert(make_pair(0, src));
		d.at(src) = 0;
		for (const auto &i : g.at(src)) {
			d.at(i.first) = i.second;
			rbtree.insert(make_pair(i.second, i.first));
		}
		while (!rbtree.empty()) {
			set<pair<int, int>>::iterator pt = begin(rbtree);
			pair<int, int> p = *pt;
			rbtree.erase(pt);
			if (p.second == dist) {
				return p.first;
			}
			for (const auto &i : g.at(p.second)) {
				if (i.second + p.first < d.at(i.first)) {
					if (!rbtree.empty() and rbtree.count(make_pair(d.at(i.first), i.first))) {
						rbtree.erase(make_pair(d.at(i.first), i.first));
					}
					d.at(i.first) = i.second + p.first;
					rbtree.insert(make_pair(d.at(i.first), i.first));
				}
			}
		}
		return INT_MAX;
	}
private:
	unordered_map<string, int> h;
	vector<vector<pair<int, int>>> g;
};

int main(void) {
	int M;
	cin >> M;
	for (int i = 0; i < M; i++) {
		int n;
		cin >> n;
		Graph graph(n);
		for (int b = 1; b <= n; b++) {
			string name;
			cin >> name;
			int k;
			cin >> k;
			for (int j = 0; j < k; j++) {
				int e, w;
				cin >> e >> w;
				graph.insert(name, b, e, w);
			}
		}
		int l;
		cin >> l;
		for (int k = 0; k < l; k++) {
			string a, b;
			cin >> a >> b;
			cout << graph.Dijkstra(a, b) << '\n';
		}
	}
	return 0;
}
